#! /usr/bin/env node

import https from 'https'
import { JSDOM } from 'jsdom'
import chalk from 'chalk'
import { getWeek } from 'date-fns'
import { tree } from '@holiday-asciis/xmas'

const getPrettyMonth = (date) => date.getMonth() === 0 ? '01' : String(date.getMonth() + 1)

const fetchPage = (url) => new Promise((resolve, reject) => https.get(url, (response) => {
    const { statusCode } = response

    if (statusCode !== 200) {
        reject(new Error(statusCode))
    }

    let html = ''

    response.on('data', (chunk) => { html += chunk })
    response.on('end', () => resolve(new JSDOM(html).window.document))
}))

const parseMenu = (html) => {
    try {
        const currentWeek = html.querySelector('#current')
        const menuItems = Array.from(currentWeek.querySelectorAll('.menu-item'))

        const parsedMenu = menuItems
            .map(row => ({
                day: row.querySelector('h6')?.textContent?.trim() ?? '=',
                food: Array.from(row.querySelectorAll('p')).map(food => food?.textContent?.trim())
            }))

        return parsedMenu
    } catch (error) {
        throw new Error(error)
    }
}

const printMenu = (menu, onlyToday) => {
    const today = new Date()
    const fix = '========================'

    console.log(chalk.bold(`Lunch v.${getWeek(today)}`))

    if (onlyToday && (today.getDay() === 0 || today.getDay() === 6)) {
        console.log(chalk.bold('NO LUNCH TODAY :('))
        return
    }

    const printToday = (day) => {
        console.log(chalk.bold(`${fix}${day.day === '=' ? day.day : ` ${day.day} `}${fix}`))
        day.food.forEach(food => food && console.log(chalk.bold(`- ${food}`)))
    }

    menu.forEach((day, index) => {
        if (onlyToday) {
            if (today.getDay() === index + 1) {
                printToday(day)
            }
        } else {
            if (today.getDay() === index + 1) {
                printToday(day)
            } else {
                console.log(`${fix}${day.day === '=' ? day.day : ` ${day.day} `}${fix}`)
                day.food.forEach(food => food && console.log(`- ${food}`))
            }

            console.log('\n')
        }
    })
}

const month = getPrettyMonth(new Date())
const fetchMenu = (onlyToday = false) => {
    fetchPage('https://www.villaoscar.se/')
        .then((response) => {
            const parsedMenu = parseMenu(response)
            printMenu(parsedMenu, onlyToday)
            console.log('\n')
            console.log(month === '12' ? `${tree(12)}` : '')
        })
        .catch(error => console.log(error))
}

const printHelp = () => {
    console.log(`
    usage: villan <command>

    where <command> is one of:

    -h, --help  quick help
    -d, --day   get todays menu
    -w, --week  get the weeks menu
    `)
}

const start = (type = '') => {
    switch (type) {
        case '-h':
        case '--help':
            printHelp()
            break
        case '-d':
        case '--day':
            fetchMenu(true)
            break
        case '-w':
        case '--week':
        default:
            fetchMenu()
            break
    }
}

start(process.argv[2])
